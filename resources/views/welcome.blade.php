<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Anisa & Reno</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="The Wedding of Rehan and Maulidan" />
	<meta name="keywords" content="wedding, wedding invitation, invitation, the wedding" />
	<meta name="author" content="Anisa & Reno" />

	<!-- Facebook and Twitter integration -->
	<meta property="og:type" content="website">
	<meta property="og:title" content="The Wedding of Anisa and Reno" />
	<meta property="og:image" content="images/readme/half%20circle-200.png" />
	<meta property="og:site_name" content="The Wedding of Anisa and Reno" />
	<meta property="og:description" content="The Wedding of Anisa and Reno. Open source wedding invitation" />

	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:title" content="The Wedding of Anisa and Reno" />
	<meta name="twitter:description" content="Open Source Wedding Invitation. The Wedding of Anisa and Reno" />
	<meta name="twitter:image" content="images/readme/half%20circle-200.png" />

    @include('style')

</head>

<body>

	<div class="fh5co-loader"></div>

	<!--<div class="container">-->
	<div id="page">
		<!--Floating button-->
		<div id="floating-button" class="float">
			<div class="control-center open">
				<div class="audio-play">
					<div data-video="_TNwN92mw6E" data-autoplay="0" data-loop="1" id="youtube-audio1"></div>
				</div>
				<ul class="right-sidebar">
					<!--gallery-->
					<li onclick="scrollToElement('fh5co-gallery')"
						style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAADe0lEQVRoge2YTWgVVxiG35Mr1n+qopYkik1dFKRiJGgWLmqrLsSFPy3Y1G6soFkJdulWcFHBgAFRQcGFLgRr1W6kLvz/SdpNQQqtfyXG+oMuaheamMfFzMXPw0zuzOTcG5H7bu7MOe/3fe878805M1eqo4463m8ADUAHcBroBwYJg8E43ylgA9BQDfEtwO+BBFfCb8DHeTW64cRLuiZpxkguQk48ktTunLuTNSDRQHw7eyW1BhKWB72SljjnhrKQ0/pug0ZHvCS1Sfo6KznNQEcYLYWRuX6agUWBhBRFW/kAmDscMc3AzKBy8mOWOb4YLyiJSDNQCqtH9yQtlzRJ0gpJ/1Tg2/rNko7k2ieqsMZ/6eVfUSkgQcv6JK3hd79kXPfOrxXI8V3SYK0MtFc4z4KFmZlVaKG7cdtMAlYC9wq00ECS1rSdmKTxWsI553wt5TGLIi10QNISRUttu6SDBTVWDyl39RXQmcLvBIZG0GJZWih7VyTkGwQ2VYj5Fhh4Fw0MAB3e/EagD9iYwcQzYAswG5gOrANu1crAC7wNBNhM1E7Ev997818BL+P52yR8pACTgfPVNvACWOONbzXiyxgCtnm8VcDNJPGGMwE4Wy0D/wMrvbEfSH9Qh4DtHr/BHI8HThB9Njaa8XHAL9Uw8Ll3viPtSnnYkZBrInDOcP4C5pj5sbG5cAY8ATszigfoAcab2CnAxQTeXeATwxsDHAtuIKf4G8A0E/shcHUY/gNgvuGXgENBDAAO6Moh/gIw2cRPjQ1VwkNggVd3bwgDB3KIPwtMMLGzgD9yxD8GWk28A/aM1MCDjMVPA+NMXBPwZw7xZTwFFifoKBnOYB4DZzIUPQV8YGLmEK0wRfEcWObpaDLzfXkMfFOh2FFgjOHPI8M7fkYTX5i89tPT/6ob1kAJ6E0pchgoGe6nwP0A4sv41eTuNuM/ZjYQBzcDd7zk+3h7h/0M+Deg+H6gOc79EfCfmVuay0CcpIU3L11dgDNzi4AnAcVfAZri3A44buZ6bO28JpzvHmgnek0eCQaAv4GfgNXEdzaut8vjLktW946BqG2Oe+J3j7auVBAtFo1Eq0030SpkcRKzYIQoOJvoRawW2I9ZqkOaaAEuVVF4Dzl6vtCTTfTArVX0d1+rpEZJRa7WK0kPJd2XdF7Sz5IuO+dG/X+pOuqoo0Z4DePtUHAz1eX1AAAAAElFTkSuQmCC)">
					</li>
					<!--chat-->
					<li onclick="scrollToElement('fh5co-testimonial')"
						style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAABKklEQVRIidWVq04DQRhGv900QUFFCVWgUYQ6PAgeAUVwSDwplocoCamsqKlE4ICQECDlAfAYoAoDHEQ7SdnO7lw6CE6yYi45384/kxnpv5NVDQJ1SXuSdiVtSGpOhl4kDSVdSOplWTYKSgVqwAnwjps3oA3UfOVN4NpDXOQKWHHJ68BDhNxwDyxVBZzPITeclclbwHeCgC9g03jzqYwDOU6VJ7mkfVvAdgK5YccWsGqZ2JC0HNA2rNkCsEwslszVNizM9ABPCTbYMLSt4LLkb2K4sQV0ZS9TDD1rL9BJUJ670ljGV8XjHPJPYKtybUADuI0MOPUqIOPr+hgYBcgHQO62/w5aBA6BPvAMvE6+In1g9uzHUpB38X1sAgM+gKOk4qmANrD+J/JQfgANB//YYXlyuAAAAABJRU5ErkJggg==)">
					</li>
				</ul>
				<!--calendar-->
				<div onclick="scrollToElement('fh5co-event')" class="option-btn open"></div>
				<ul class="left-sidebar">
					<!--countdown-->
					<li onclick="scrollToElement('fh5co-header')"
						style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAABG0lEQVRIidXVTS4EQRjG8SZ2IrEnETEcQmYsjLkCiUu4g1P42FpxhmGG2IvEcAcfG9Z+FtOS1qaramZ6wZPUpqve5//2k36rs+wvCLd+6yaldjaR8TmKm95iDcIhzrA4bmEHp3jCR74ecYKdwrm7PLoBllOMN9AfkX1ZPaxjBff5s4OY+RbeEsy/9YoWFrCP+VjnVeabaAYgjZRoLqvaLJypUi9m3gnlkACAdtGzPAd70VeMazcEaNYA2AoBlmoA/JiBudJmcPzRzLJsJgKo9jCc1mk1KHqWI7qOdJeifghwXgPgIriL7hTxXEXxWMXzBOZpV0UOaeUFqXox/MLShYbAvVRQF2tjmZdAbRwb/kje8/WAI2xPbPxv9AUiVMwGoTr9YAAAAABJRU5ErkJggg==)">
					</li>
					<!--couple-->
					<li onclick="scrollToElement('fh5co-couple')"
						style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABmJLR0QA/wD/AP+gvaeTAAAB7UlEQVRIie2VPUiVURjH/+eqSSJN4tDWRRAH5xD8QDCwgqAWhwwSaarBwUGaHFwcBAchGqNNdDaKoKAkBEvUURAVRInw82Jx0Z/De64+Xu5773nv0CD3P533eZ//+T3nW6qoomstwAEPgXfAKnAA7AE/gWGgJsbXAIwCi8ARkAF+AS+AqlLQJmCB4voMDAL1xvfUFxin98WgLX5kIdoHbnvfK+AswJMBvgGPLbQGWAmEAgx7XzuQTeDLaSgHfpLA9Bu45X0/yoAC/AXSKUn9AfvuTNIXSb3OuUOgQ9LdAJ8kfZDUJenYf9dKGqiW1JyXuCbpq6RlSRuStiStO+f2TM6DQOiSpOfOuV1gStKIj3eKyx2ZJdqxrlRvwHzMNP4DlomOYx/m+AEdJm9H5mM8cBQCNvKA00AbcKOIp9GuswW3JgBnjO9toKfWVmrBdQnAVkkKvlBKUsbHY6ephNbKMaUkffftj0C6jD5uhiRhrtlcoAXY9jPwJrATq+5AT+eVNfbBZqLjdAq0JwTPBOQ7YM54Nu3PMR/8A9wrYH4UAwZ4VgSaBmYtFOi1CSlgxiTMA5PAhG9jcrfywFngJQGXT1x1VcBrosc8X/sm734BOERPX9AdXrBCoh3YI+mOogdiXdIn59xJWSOqqKL/qXMXI9MKbss7gAAAAABJRU5ErkJggg==)">
					</li>
				</ul>
			</div>
		</div>

		<div id="overlay">
			<div class="content">
				<div class="container">
					<div class="row">
						<div
							class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box fadeInUp animated-fast">
							<img src="{{ asset('template/images/oval.png') }}" alt="" class="couple-main">
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
							<h1>Anisa & Reno</h1>
							<p style="margin-bottom: 0">Kepada Bapak/Ibu/Saudara/i</p>
							<p>Kami Mengundang Anda Untuk Hadir Di Acara Pernikahan Kami.</p>

							<button class="btn btn-primary" id="open-invitation">Buka Undangan</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-header">
			<div class="container">
				<img class="flower-1" style="position: absolute; right: 0; top: 0"
					src="{{ asset('template/images/background/flowers/top-right-1.svg') }}">
				<img class="flower-1" style="position: absolute; left: -25px; top:0"
					src="{{ asset('template/images/background/flowers/top-left-1.svg') }}">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<h2>The Wedding Of</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<img src="{{ asset('template/images/reno.jpeg') }}" alt="" class="couple-main">
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<h1>Anisa & Reno</h1>
						<p style="color: #bf9b73">Kami berharap Anda menjadi bagian dari hari istimewa kami!</p>
						<div class="simply-countdown simply-countdown-wedding"></div>
						<br>
						<p>
							<a href="https://calendar.google.com/event?action=TEMPLATE&tmeid=NW0zY2Ewam91bnUzbW5ub2YwdjVxaTliNmUgcDY4MjI3cmY0dTJ0cWI1b3JrZjEwbTN1NDRAZw&tmsrc=p68227rf4u2tqb5orkf10m3u44%40group.calendar.google.com"
								target="_blank" class="btn btn-primary btn-sm">Save the date</a><br>
							<i style="font-size: 15px">*Klik tombol ini untuk menyimpan tanggal pada google kalender</i>
						</p>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-couple"
			style="background: url('images/background/repeat-background/sage-2.jpg'); background-repeat:repeat ">
			<div class="container">
				<img class="flower-2-right" style="position: absolute;" src="{{ asset('template/images/background/flowers/right-1.svg') }}">
				<img class="flower-2-left" style="position: absolute;" src="{{ asset('template/images/background/flowers/left-1.svg') }}">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<h2>Assalamu'alaikum Wr. Wb.</h2>
						<p style="color: black">Tanpa mengurangi rasa hormat. Kami mengundang Bapak/Ibu/Saudara/i serta
							Kerabat sekalian untuk menghadiri acara pernikahan kami:</p>
					</div>
				</div>
				<div class="couple-wrap animate-box">
					<div class="couple-half">
						<div class="groom">
							<img src="{{ asset('template/images/anisa.png') }}" alt="groom" class="img-responsive">
						</div>
						<div class="desc-groom">
							<h3>Anisa Rezekiyani</h3>
							<p><span style="color: black">Putri Keempat dari</span><br>
								<span class="parents-font">Bapak Ahmad kirom</span>
								<br>
								<span class="parents-font">Ibu Sri Mulyani</span>
							</p>
							<div id="social-media-rehan">
								<ul class="fh5co-social-icons">
									<li><a target="_blank" href="#"><i
												class="icon-instagram-with-circle"></i></a></li>
									<li><a target="_blank" href="https://www.linkedin.com/in/rayhanyulanda/"><i
												class="icon-facebook-with-circle"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<p class="heart text-center"><i class="icon-heart2"></i></p>
					<div class="couple-half">
						<div class="bride">
							<img src="{{ asset('template/images/reno1.png') }}" alt="groom" class="img-responsive">
						</div>
						<div class="desc-bride">
							<h3>Reno Junianto</h3>
							<p><span style="color: black">Putra Pertama dari</span><br>
								<span class="parents-font">Bapak Casmuri</span>
								<br>
								<span class="parents-font">(almh) Ibu Cucu Suciaty </span>
							</p>
							<div id="social-media-molid">
								<ul class="fh5co-social-icons">
									<li><a target="_blank" href="https://github.com/maulidandev"><i
												class="icon-instagram-with-circle"></i></a></li>
									<li><a target="_blank" href="https://www.linkedin.com/in/maulidandev/"><i
												class="icon-facebook-with-circle"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--	style="background-image:url('images/background/blue-brush-2.jpg');"-->
		<div id="fh5co-event" class="fh5co-bg" style="border-top: 1px solid #f2f2f2">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<h2>Save The Date <a href="{{ asset('template/calendar.ics') }}" class="btn btn-info"><i class="icon-download2"></i></a>
						</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="col-md-6 col-sm-6 text-center">
							<div class="event-wrap animate-box">
								<h3 style="font-family: 'Oswald', Arial, serif;">Akad Nikah</h3>
								<div class="event-col">
									<i class="icon-clock"></i>
									<span>10:00</span>
									<span>Selesai</span>
								</div>
								<div class="event-col">
									<i class="icon-calendar"></i>
									<span>Minggu</span>
									<span class="tanggal">29 October 2023</span>
								</div>
								<p style="font-family: 'Courgette', Arial, serif">Alamat : Kediaman Mempelai Wanita :
									Jl.Tanjung Pura V No.47 Rt2/Rw5, Pegadungan, Kec. Kalideres, Kota Jakarta Barat,
									Daerah Khusus Ibukota Jakarta 11830</p>

								<a href="https://maps.app.goo.gl/RjpksmUWBrhY9bGY6" target="_blank"
									class="btn btn-primary">Penunjuk Lokasi <i class="icon-map2"></i></a>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 text-center">
							<div class="event-wrap animate-box">
								<h3 style="font-family: 'Oswald', Arial, serif;">Resepsi</h3>
								<div class="event-col">
									<i class="icon-clock"></i>
									<span>11:00</span>
									<span>Selesai</span>
								</div>
								<div class="event-col">
									<i class="icon-calendar"></i>
									<span>Minggu</span>
									<span class="tanggal">29 October 2023</span>
								</div>
								<p style="font-family: 'Courgette', Arial, serif">Alamat : Kediaman Mempelai Wanita :
									Jl.Tanjung Pura V No.47 Rt2/Rw5, Pegadungan, Kec. Kalideres, Kota Jakarta Barat,
									Daerah Khusus Ibukota Jakarta 11830</p>

								<a href="hhttps://maps.app.goo.gl/RjpksmUWBrhY9bGY6" target="_blank"
									class="btn btn-primary">Penunjuk Lokasi <i class="icon-map2"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-gallery"
			style="background: url('{{ asset('template/images/background/repeat-background/sage-2.jpg') }}'); background-repeat:repeat ">
			<img class="flower-3-top" src="{{ asset('template/images/background/flowers/top-2.svg') }}">
			<img class="flower-4-bottom" src="{{ asset('template/images/background/flowers/bottom-2.svg') }}">

			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<h2>Galeri Foto</h2>
						<h3 style="color: black">Anisa & Reno</h3>
					</div>
				</div>
				<div class="row row-bottom-padded-md animate-box">
					<div class="col-md-12">
						<div id="inline-gallery-container" class="inline-gallery-container"
							style="position: relative; height: 500px"></div>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-ayat">
			<img height="400" width="100%" style="position: absolute; bottom: -175px"
				src="{{ asset('template/images/background/wave-down.svg') }}">
			<div class="container">
				<div class="ayat-content animate-box">
					Dan di antara tanda-tanda kekuasaan-Nya lah Dia menciptakan untukmu istri-istri dari jenismu
					sendiri, supaya kamu cenderung dan merasa tenteram kepadanya, dan dijadikan-Nya diantaramu rasa
					kasih dan sayang. Sesungguhnya pada yang demikian itu benar-benar terdapat tanda-tanda bagi kaum
					yang berfikir.
					<br>
					<b>QS Ar Rum:21</b>
				</div>
			</div>
		</div>
        <!--	style="background-image:url('images/background/blue-brush-2.jpg');"-->
		<div id="fh5co-event" class="fh5co-bg" style="border-top: 1px solid #f2f2f2">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<h2>Tanda Kasih <img src="{{ asset('template/images/BCA.png') }}" width="200">
						</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="col-md-6 col-sm-6 text-center">
							<div class="event-wrap animate-box">
								<h3 style="font-family: 'Oswald', Arial, serif;">Anisa</h3>
                                <br>
                                <h4 id="copyText">Sajds</h4>
                                <br />
								<button onclick="copyToClipboard()"
									class="btn btn-light mt-4">Salin</button>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 text-center">
							<div class="event-wrap animate-box">
								<h3 style="font-family: 'Oswald', Arial, serif;">Reno</h3>
                                <br>
                                <h4 id="copyText2">asda</h4>
                                <br />
								<button onclick="copyToClipboard2()"
									class="btn btn-light mt-4">Salin</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-testimonial"
			style="background: url('{{ asset('template/images/background/repeat-background/so-white.png') }}'); background-repeat:repeat; border-top: 1px solid #f2f2f2">
			<img height="400" class="flower-bukutamu-right" style="right: 0;top: 0;position: absolute;"
				src="{{ asset('template/images/background/flowers/top-right-3.svg') }}">
			<img height="400" class="flower-bukutamu-left" style="position: absolute; left: 0; bottom: 0"
				src="{{ asset('template/images/background/flowers/bottom-left-3.svg') }}">
			<div class="container">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
						<h2>Buku Tamu & Ucapan</h2>
						<span style="color: #bf9b73">Doa Restu Anda merupakan karunia yang sangat berarti bagi
							kami.</span>
					</div>
				</div>
			<div class="row">
                <div class="col-md-12 animate-box">
                    <div class="wrap-testimony">
                        <div class="owl-carousel-fullwidth">
                            <div class="item">
                                <div class="testimony-slide active text-center">
                                    <span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
                                    <blockquote>
                                        <p>"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics"</p>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimony-slide active text-center">
                                    <figure>
                                        <img src="{{ asset('template/images/couple-2.jpg') }}" alt="user">
                                    </figure>
                                    <span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
                                    <blockquote>
                                        <p>"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, at the coast of the Semantics, a large language ocean."</p>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimony-slide active text-center">
                                    <figure>
                                        <img src="{{ asset('template/images/couple-3.jpg') }}" alt="user">
                                    </figure>
                                    <span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
                                    <blockquote>
                                        <p>"Far far away, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean."</p>
                                    </blockquote>
                                </div>
                            </div>
							<div class="item">
                                <div class="testimony-slide active text-center">
                                    <figure>
                                        <img src="{{ asset('template/images/couple-3.jpg') }}" alt="user">
                                    </figure>
                                    <span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
                                    <blockquote>
                                        <p>"Far far away, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean."</p>
                                    </blockquote>
                                </div>
                            </div>
							<div class="item">
                                <div class="testimony-slide active text-center">
                                    <figure>
                                        <img src="{{ asset('template/images/couple-3.jpg' )}}" alt="user">
                                    </figure>
                                    <span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
                                    <blockquote>
                                        <p>"Far far away, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean."</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

				<form action="POST">
					<label>Nama</label>
					<input type="text" class="form-control">
					<br>
				</form>
				<div id="button-send" class="text-center animate-box">
					<a href="https://github.com/maulidandev/nikah-rehan-maulidan/issues/new/choose" target="_blank"
						class="btn btn-primary btn-sm">Kirim Ucapan</a>
				</div>
			</div>
		</div>

		<footer id="fh5co-footer" role="contentinfo"
			style="background: url('{{ asset('template/images/background/repeat-background/witewall_3.png') }}'); background-repeat:repeat ">
			<div class="container">
				<img class="flower-1" style="position: absolute; right: 0; bottom: 0"
					src="{{ asset('template/images/background/flowers/bottom-right-1.svg' )}}">
				<img class="flower-1" style="position: absolute; left: 0; bottom:0"
					src="{{ asset('template/images/background/flowers/bottom-left-1.svg') }}">
				<div class="row copyright animate-box">
					<div class="col-md-12 text-center">
						<p>
							Merupakan suatu kehormatan dan kebahagiaan bagi kami, apabila Bapak/Ibu/Saudara/i berkenan
							hadir dan memberikan doa restu.
							<br>Atas kehadiran dan doa restunya, kami mengucapkan terima kasih.
						<h3 style="color: #bf9b73">Wassalamu'alaikum Wr. Wb.</h3>
						Jazakumullahu Khairan
						</p>
						<img src="{{ asset('template/images/oval.png' )}}" width="100" height="100">
						<br>
						<h1 style="font-family: 'Satisfy', Arial, serif">Anisa & Reno</h1>
						<p>
							© Copyright 2022 NIKAH DIGITAL All Rights Reserved </br>
							<small>Instagram : nikahdigitalcom | Whatsapp : (+62) blank| gatau.com
								|halo@males.com</small>
						<ul class="fh5co-social-icons">
							<li><a target="_blank" href="https://wa.me/6285360061021/"><i class="icon-phone"></i></a>
							</li>
							<li><a target="_blank" href="https://nikahdigital.com/"><i class="icon-facebook"></i></a></li>
							<li><a target="_blank" href="https://www.instagram.com/nikahdigitalcom/"><i
										class="icon-instagram"></i></a></li>
						</ul>
						</p>
					</div>
				</div>

			</div>
		</footer>
	</div>
	<!--</div>-->

	<!--<div class="gototop js-top">
	<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>-->

	@include('script')

</body>

</html>