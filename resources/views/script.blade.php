<!-- jQuery -->
<script src="{{ asset('template/js/jquery.min.js') }}"></script>
	<!-- jQuery Easing -->
	<script src="{{ asset('template/js/jquery.easing.1.3.js') }}"></script>
	<!-- Bootstrap -->
	<script src="{{ asset('template/js/bootstrap.min.js') }}"></script>
	
	<!-- Waypoints -->
	<script src="{{ asset('template/js/jquery.waypoints.min.js') }}"></script>
	<!-- Carousel -->
	<script src="{{ asset('template/js/owl.carousel.min.js') }}"></script>
	<!-- countTo -->
	<script src="{{ asset('template/js/jquery.countTo.js') }}"></script>

	<!-- Stellar -->
	<script src="{{ asset('template/js/jquery.stellar.min.js') }}"></script>
	<!-- Magnific Popup -->
	<script src="{{ asset('template/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('template/js/magnific-popup-options.js') }}"></script>

	<!-- // <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.js"></script> -->
	<script src="{{ asset('template/js/simplyCountdown.js') }}"></script>
	<!-- Main -->
	<script src="{{ asset('template/js/main.js') }}"></script>

	<script src="{{ asset('template/lib/lightgallery.js/dist/lightgallery.min.js') }}"></script>
	<script src="{{ asset('template/lib/lightgallery.js/dist/plugins/thumbnail/lg-thumbnail.min.js') }}"></script>
	<script src="{{ asset('template/lib/lightgallery.js/dist/plugins/autoplay/lg-autoplay.min.js') }}"></script>
	<script src="{{ asset('template/lib/lightgallery.js/dist/plugins/fullscreen/lg-fullscreen.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/showdown/2.0.3/showdown.min.js"></script>

	<script>
		// Tanggal pernikahan
		var weddingDate = new Date('2023-10-29T18:00:00');

		simplyCountdown('.simply-countdown-wedding', {
			year: weddingDate.getFullYear(),
			month: weddingDate.getMonth() + 1,
			day: weddingDate.getDate(),
			hours: weddingDate.getHours(),
			minutes: weddingDate.getMinutes(),
			seconds: weddingDate.getSeconds(),
			words: {
				days: 'Hari',
				hours: 'Jam',
				minutes: 'Menit',
				seconds: 'Detik',
				pluralLetter: ''
			},
			plural: true
		});

		//jQuery example
		// $('#simply-countdown-losange').simplyCountdown({
		// 	year: d.getFullYear(),
		// 	month: d.getMonth() + 1,
		// 	day: d.getDate(),
		// 	enableUtc: false
		// });

		$('#open-invitation').click(function () {
			$('#overlay').addClass('hide-overlay');
			playAudio1();
			/*yang bagian floating button ada animasinya tapi karena ketutup dulu sama overlay jadinya gak nampak*/
			/*pengen pas di klik buka undangan baru jalan animasinya*/
			/*var c = document.getElementsByClassName('right-sidebar');
			for (var i = 0; i < c.length; i++) {
				c[i].classList.add('animate');
			}*/
		});

		function scrollToElement(el) {
			$("html, body").animate({ scrollTop: $(`#${el}`).offset().top }, 1000);
		}
	</script>

	<script>
		// 2. This code loads the IFrame Player API code asynchronously.
		var tag = document.createElement('script');

		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		// 3. This function creates an <iframe> (and YouTube player)
		//    after the API code downloads.
		var player1;
		function onYouTubeIframeAPIReady() {

			var ctrlq1 = document.getElementById("youtube-audio1");
			ctrlq1.innerHTML = '<img id="youtube-icon1" src=""/><div id="youtube-player1"></div>';
			ctrlq1.style.cssText = 'width:20px;margin:0 auto;cursor:pointer;cursor:hand;display:none';
			ctrlq1.onclick = toggleAudio1;

			player1 = new YT.Player('youtube-player1', {
				height: '0',
				width: '0',
				videoId: ctrlq1.dataset.video,
				playerVars: {
					autoplay: ctrlq1.dataset.autoplay,
					loop: ctrlq1.dataset.loop,
				},
				events: {
					'onReady': onPlayerReady1,
					'onStateChange': onPlayerStateChange1
				}
			});
		}

		function togglePlayButton1(play) {
			document.getElementById("youtube-icon1").src = play ? "https://ngodingsolusi.github.io/the-wedding-of-rehan-maulidan/images/audio/pause.png" : "https://ngodingsolusi.github.io/the-wedding-of-rehan-maulidan/images/audio/play.png";
		}

		function toggleAudio1() {
			if (player1.getPlayerState() == 1 || player1.getPlayerState() == 3) {
				player1.pauseVideo();
				togglePlayButton1(false);
			} else {
				player1.playVideo();
				togglePlayButton1(true);
			}
		}

		function playAudio1() {
			if (!(player1.getPlayerState() == 1 || player1.getPlayerState() == 3)) {
				player1.playVideo();
				togglePlayButton1(true);
			}
		}

		function onPlayerReady1(event) {
			player1.setPlaybackQuality("small");
			document.getElementById("youtube-audio1").style.display = "block";
			togglePlayButton1(player1.getPlayerState() !== 5);
		}

		function onPlayerStateChange1(event) {
			if (event.data === 0) {
				togglePlayButton1(false);
			}
		}
	</script>

	<script>
		function akadLocation() {
			// If it's an iPhone..
			if ((navigator.platform.indexOf("iPhone") != -1)
				|| (navigator.platform.indexOf("iPod") != -1)
				|| (navigator.platform.indexOf("iPad") != -1))
				window.open("maps://www.google.com/maps?daddr=masjid+keuchik+leumik");
			else
				window.open("https://www.google.com/maps?daddr=masjid+keuchik+leumik");
		}

		function walimahLocation() {
			// If it's an iPhone..
			if ((navigator.platform.indexOf("iPhone") != -1)
				|| (navigator.platform.indexOf("iPod") != -1)
				|| (navigator.platform.indexOf("iPad") != -1))
				window.open("maps://www.google.com/maps?daddr=Amel+Convention+Hall");
			else
				window.open("https://www.google.com/maps?daddr=Amel+Convention+Hall");
		}
	</script>

	<script type="text/javascript">
		const lgContainer = document.getElementById('inline-gallery-container');
		const inlineGallery = lightGallery(lgContainer, {
			container: lgContainer,
			dynamic: true,
			thumbnail: true,
			// Turn off hash plugin in case if you are using it
			// as we don't want to change the url on slide change
			hash: false,
			// Do not allow users to close the gallery
			closable: false,
			// Add maximize icon to enlarge the gallery
			showMaximizeIcon: true,
			download: false,
			// Append caption inside the slide item
			// to apply some animation for the captions (Optional)
			appendSubHtmlTo: '.lg-item',
			// Delay slide transition to complete captions animations
			// before navigating to different slides (Optional)
			// You can find caption animation demo on the captions demo page
			slideDelay: 400,
			dynamicEl: [
				{
					src: '{{ asset('template/images/gallery/gal1.jpeg') }}',
					thumb: '{{ asset('template/images/gallery/gal1.jpeg') }}',
					/*subHtml: `<div class="lightGallery-captions">
						<h4>Caption 1</h4>
						<p>Description of the slide 1</p>
					</div>`,*/
				}, {
					src: '{{ asset('template/images/gallery/gal2.jpeg') }}',
					thumb: '{{ asset('template/images/gallery/gal2.jpeg') }}',
					/*subHtml: `<div class="lightGallery-captions">
						<h4>Caption 1</h4>
						<p>Description of the slide 1</p>
					</div>`,*/
				}, {
					src: '{{ asset('template/images/gallery/gal3.jpeg') }}',
					thumb: '{{ asset('template/images/gallery/gal3.jpeg') }}',
					/*subHtml: `<div class="lightGallery-captions">
						<h4>Caption 1</h4>
						<p>Description of the slide 1</p>
					</div>`,*/
				}, {
					src: '{{ asset('template/images/gallery/gal4.jpeg') }}',
					thumb: '{{ asset('template/images/gallery/gal4.jpeg') }}',
					/*subHtml: `<div class="lightGallery-captions">
						<h4>Caption 1</h4>
						<p>Description of the slide 1</p>
					</div>`,*/
				}, {
					src: '{{ asset('template/images/gallery/gal5.jpeg') }}',
					thumb: '{{ asset('template/images/gallery/gal5.jpeg') }}',
					/*subHtml: `<div class="lightGallery-captions">
						<h4>Caption 1</h4>
						<p>Description of the slide 1</p>
					</div>`,*/
				}, {
					src: '{{ asset('template/images/gallery/gal6.jpeg') }}',
					thumb: '{{ asset('template/images/gallery/gal6.jpeg') }}',
					/*subHtml: `<div class="lightGallery-captions">
						<h4>Caption 1</h4>
						<p>Description of the slide 1</p>
					</div>`,*/
				}
			],
		});

		// Since we are using dynamic mode, we need to programmatically open lightGallery
		inlineGallery.openGallery();
	</script>
    <script>
        function copyToClipboard() {
            var copyText = document.getElementById("copyText");
            var textToCopy = copyText.textContent; // Mengambil teks dari elemen h4
            var tempInput = document.createElement("input");
            document.body.appendChild(tempInput);
            tempInput.setAttribute("value", textToCopy);
            tempInput.select();
            document.execCommand("copy");
            document.body.removeChild(tempInput);
            alert("Teks telah disalin: " + textToCopy);
        }
        function copyToClipboard2() {
            var copyText = document.getElementById("copyText2");
            var textToCopy = copyText.textContent; // Mengambil teks dari elemen h4
            var tempInput = document.createElement("input");
            document.body.appendChild(tempInput);
            tempInput.setAttribute("value", textToCopy);
            tempInput.select();
            document.execCommand("copy");
            document.body.removeChild(tempInput);
            alert("Teks telah disalin: " + textToCopy);
        }

    </script>