
	<link rel="icon" href="{{ asset('template/images/oval.png') }}">

<!--
  //////////////////////////////////////////////////////

  FREE HTML5 TEMPLATE
  DESIGNED & DEVELOPED by FREEHTML5.CO

  Website: 		http://freehtml5.co/
  Email: 			info@freehtml5.co
  Twitter: 		http://twitter.com/fh5co
  Facebook: 		https://www.facebook.com/fh5co

  //////////////////////////////////////////////////////
   -->

<link href='https://fonts.googleapis.com/css?family=Didact+Gothic' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Estonia" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">

<!-- Animate.css -->
<link rel="stylesheet" href="{{ asset('template/css/animate.css') }}">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="{{ asset('template/css/icomoon.css') }}">
<!-- Bootstrap  -->
<link rel="stylesheet" href="{{ asset('template/css/bootstrap.css') }}">


<!-- Magnific Popup -->
<link rel="stylesheet" href="{{ asset('template/css/magnific-popup.css') }}">

<!-- Owl Carousel  -->
<link rel="stylesheet" href="{{ asset('template/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('template/css/owl.theme.default.min.css') }}">

<!--<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.5.0-beta.2/css/lightgallery-bundle.min.css" />-->
<link type="text/css" rel="stylesheet" href="{{ asset('template/lib/lightgallery.js/dist/css/lightgallery-bundle.min.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('template/lib/lightgallery.js/dist/css/lg-thumbnail.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('template/lib/lightgallery.js/dist/css/lg-autoplay.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('template/lib/lightgallery.js/dist/css/lg-fullscreen.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('template/lib/lightgallery.js/dist/css/lg-transitions.css') }}" />

<!-- Theme style  -->
<link rel="stylesheet" href="{{ asset('template/css/style.css') }}">

<!-- Modernizr JS -->
<script src="{{ asset('template/js/modernizr-2.6.2.min.js') }}"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<![endif]-->